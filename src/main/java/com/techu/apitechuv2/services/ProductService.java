package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll()  {
        return this.productRepository.findAll();

    }
    public ProductModel add(ProductModel productModel){
        return this.productRepository.save (productModel);
    }

    public Optional<ProductModel> findById (String id){
        System.out.println("findById");
        System.out.println("Obteniendo el producto con la id:"  + id);

        return this.productRepository.findById(id);
    }

    public ProductModel update(ProductModel productModel){
        return this.productRepository.save (productModel);
    }

    public boolean delete (String id){
        System.out.println("entro en Servicio delete");
        boolean result = false;

        if (this.productRepository.findById(id).isPresent()) {
            System.out.println("producto encontrado");
            this.productRepository.deleteById(id);
            result = true;
        }

       return result;
    }
}
