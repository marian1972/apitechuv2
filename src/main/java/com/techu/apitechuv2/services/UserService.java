package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    @Autowired
    UserRepository UserRepository;

    public List<UserModel> findAll(String field)  {
        System.out.println("entramos en servicio findAll");

        if (field != null){
            System.out.println("se pide ordenación");
            return this.UserRepository.findAll(Sort.by("age"));
        }
        else {
            return this.UserRepository.findAll();
        }

   }

    public UserModel add(UserModel user){
        return this.UserRepository.save (user);
    }

    public Optional<UserModel> findById (String id){
        System.out.println("findById");
        System.out.println("Obteniendo el User con la id:"  + id);

        return this.UserRepository.findById(id);
    }

    public UserModel update (UserModel user){
        return this.UserRepository.save (user);
    }

    public boolean delete (String id){
        System.out.println("entro en Servicio delete");
        boolean result = false;

        if (this.UserRepository.findById(id).isPresent()) {
            System.out.println("Usuario encontrado");
            this.UserRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
