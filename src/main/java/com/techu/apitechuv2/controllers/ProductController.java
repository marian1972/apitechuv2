package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.services.ProductService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")

public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping ("/products")
    public List<ProductModel> getProducts(){
        System.out.println("getProducts");

        return this.productService.findAll();

    }
    @GetMapping ("/products/{id}")
    public ResponseEntity<Object> getProductById (@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es: " + id);

        Optional<ProductModel> result = this.productService.findById(id);

  /*
        if (result.isPresent() == true){
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }

*/

        //OPERADOR TERNARIO; sustituye a bloque If...Else
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping ("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct");
        System.out.println("La id del producto que se va a crear es:" + product.getId());
        System.out.println("La descripción del producto que se va a crear es: " + product.getDesc());
        System.out.println("El precio del producto que se va a crear es: " + product.getPrice());

        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
    }


    @PutMapping ("/products/{id}")
    public ResponseEntity<Object> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("id recibido por parámetro : " + id);
        System.out.println("El id del producto actualizado: " + product.getId());
        System.out.println(" La descripción del producto es: " + product.getDesc());
        System.out.println("El precio del producto es: " + product.getPrice());

        Optional<ProductModel> result = this.productService.findById(id);

        if (result.isPresent()) {
            System.out.println("se actualiza producto : " + id);
            return new ResponseEntity<>(this.productService.update (product), HttpStatus.CREATED);
        }
        else {
            System.out.println("el producto no puede actualizarse, no existe : " + id);
            return new ResponseEntity<>("Producto No Encontrado", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping ("/products/{id}")
    public ResponseEntity<String> deleteProduct (@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("El Id del producto a borrar es: " + id);

        boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "Producto borrado" : "Producto no borrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}

