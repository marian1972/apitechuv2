package com.techu.apitechuv2.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public ResponseEntity<String> index() {
        return new ResponseEntity<>("Hola Mundo desde Api Tech U v2 !!!", HttpStatus.OK);
    }

    @RequestMapping("/hello")
    //Al poner RequestParam quiere decir que eel parámetro va a venir de "fuera" y podemos poner un valor por defecto

    public String hello(@RequestParam(value="name", defaultValue = "Tech U v2") String name){
        //sustituye y lo formatea, diferente de poner sólo "+" que sólo concatena
        return String.format ("Hola %s!", name);
    }

}
