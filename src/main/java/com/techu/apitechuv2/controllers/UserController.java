package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/apitechu/v2")

// eso se necesita cuando nos conectemos con un front desde otra máquina. Ejemplo, desde cualquier origen se permiten métodos GET y POST
// @CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})

public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam (name="$orderby", required = false)  String field){
        System.out.println("getUsers");
        System.out.println(field);
        return new ResponseEntity<>(this.userService.findAll(field), HttpStatus.OK);
       // return this.userService.findAll(field);

    }
    @GetMapping ("/users/{id}")
    public ResponseEntity<Object> getUserById (@PathVariable String id) {
        System.out.println("getUserById");
        System.out.println("La id del Usuario a buscar es: " + id);

        Optional<UserModel> retrieveUser = this.userService.findById(id);

        if (retrieveUser.isPresent()){
            return new ResponseEntity<>(retrieveUser.get(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
        }

     /* return new ResponseEntity<>(
            result.isPresent() ? retrieve.get() : "Usuario no encontrado",
            result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND); */
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel User) {
        System.out.println("addUser");
        System.out.println("El id del Usuario que se va a crear es:" + User.getId());
        System.out.println("El nombre del Usuario que se va a crear es: " + User.getName());
        System.out.println("La Edad del Usuario que se va a crear es: " + User.getAge());

        return new ResponseEntity<>(this.userService.add(User), HttpStatus.CREATED);
    }


    @PutMapping ("/users/{id}")
    public ResponseEntity<Object> updateUser(@RequestBody UserModel User, @PathVariable String id) {
        System.out.println("updateUser");
        System.out.println("id recibido por parámetro : " + id);
        System.out.println("El id del Usuario actualizado: " + User.getId());
        System.out.println(" El nombre del Usuario es: " + User.getName());
        System.out.println("La edad del usuario es: " + User.getAge());

        if (!User.getId().equals(id)){
           System.out.println("El id indicado no coincide con lo recibido como Body");
           return new ResponseEntity<>("El id indicado no coincide con lo recibido como Body", HttpStatus.NOT_ACCEPTABLE);
        }
        else {
            Optional<UserModel> result = this.userService.findById(id);

            if (result.isPresent()) {
                System.out.println("se actualiza Usuario : " + id);
                return new ResponseEntity<>(this.userService.update(User), HttpStatus.CREATED);
            } else {
                System.out.println("el Usuario no puede actualizarse, no existe : " + id);
                return new ResponseEntity<>("Usuario No Encontrado", HttpStatus.NOT_FOUND);
            }
        }

    }

    @DeleteMapping ("/users/{id}")
    public ResponseEntity<String> deleteUser (@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("El Id del User a borrar es: " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
 }
