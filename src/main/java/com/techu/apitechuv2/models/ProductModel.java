package com.techu.apitechuv2.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document (collection = "products")

    public class ProductModel {

        //esto nos indica que éste va a ser el identificador y que no puede ser nulo
        @Id
        private String id;
        private String desc;
        private float price;

    public ProductModel() {}

    public ProductModel(String id, String desc, float price) {
        this.id = id;
        this.desc = desc;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public float getPrice() {
        return price;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}



